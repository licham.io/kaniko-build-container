FROM gcr.io/kaniko-project/executor:v1.19.2-debug

ADD files/ /

RUN cat /kaniko/ssl/certs/micham-mooneyham-root.crt >> /kaniko/ssl/certs/ca-certificates.crt

# Kaniko Build Container

This repository produces a Kaniko build container that is correctly set up for building images for licham.io. It is meant to be used as a CI image in other repositories.

It contains the following customizations:

- The Micham-Mooneyham root CA cert (required for trusting Harbor)
